/*
Syn's AyyWare Framework 2015
*/

#include "Menu.h"
#include "Controls.h"
#include "Hooks.h" // for the unload meme
#include "Interfaces.h"
#include "CRC32.h"

#define WINDOW_WIDTH 725
#define WINDOW_HEIGHT 660

AyyWareWindow Menu::Window;


void Unloadbk()
{
	DoUnload = true;
}


void SaveCallbk()
{
	switch (Menu::Window.MiscTab.SetNr.GetIndex())
	{
	case 0:
		GUI.SaveWindowState(&Menu::Window, "legit.xml");
		break;
	case 1:
		GUI.SaveWindowState(&Menu::Window, "Legit-2.xml");
		break;
	case 2:
		GUI.SaveWindowState(&Menu::Window, "rage.xml");
		break;
	case 3:
		GUI.SaveWindowState(&Menu::Window, "HVH-1.xml");
		break;
	case 4:
		GUI.SaveWindowState(&Menu::Window, "HVH-2.xml");
		break;
	}

}
void LoadCallbk()
{
	switch (Menu::Window.MiscTab.SetNr.GetIndex())
	{
	case 0:
		GUI.LoadWindowState(&Menu::Window, "legit.xml");
		break;
	case 1:
		GUI.LoadWindowState(&Menu::Window, "legit-2.xml");
		break;
	case 2:
		GUI.LoadWindowState(&Menu::Window, "rage.xml");
		break;
	case 3:
		GUI.LoadWindowState(&Menu::Window, "HVH-1.xml");
		break;
	case 4:
		GUI.LoadWindowState(&Menu::Window, "HVH-2.xml");
		break;
	}
}



void AyyWareWindow::Setup()
{
	SetPosition(50, 50);
	SetSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	SetTitle("paradise | build: 1");

	RegisterTab(&RageBotTab);
	//RegisterTab(&LegitBotTab);
	RegisterTab(&VisualsTab);
	RegisterTab(&MiscTab);
	//RegisterTab(&GlovesChanger);
	//RegisterTab(&SkinchangerTab);

	RECT Client = GetClientArea();
	Client.bottom -= 29;

	RageBotTab.Setup();
	//LegitBotTab.Setup();
	VisualsTab.Setup();
	MiscTab.Setup();
	//GlovesChanger.Setup();
	//SkinchangerTab.Setup();

}


void CRageBotTab::Setup()
{
	SetTitle("A");

#pragma region Aimbot

	//AimbotLabel.SetPosition(75, 30);
	//AimbotLabel.SetText("Active");
	//RegisterControl(&AimbotLabel);

	//Active.SetFileId("active");
	//Active.SetPosition(60, 30);
	//RegisterControl(&Active);

	AimbotGroup.SetPosition(45, 53);
	AimbotGroup.SetText("Aimbot");
	AimbotGroup.SetSize(315, 495);
	RegisterControl(&AimbotGroup);

	AimbotEnable.SetFileId("aim_enable");
	AimbotGroup.PlaceLabledControl("Aimbot", this, &AimbotEnable);

	AimbotFriendlyFire.SetFileId("tgt_friendlyfire");
	AimbotGroup.PlaceLabledControl("Friendly Fire", this, &AimbotFriendlyFire);

	AimbotAutoFire.SetFileId("aim_autofire");
	AimbotGroup.PlaceLabledControl("Auto Fire", this, &AimbotAutoFire);

	AimbotAutoWall.SetFileId("acc_awall");
	AimbotGroup.PlaceLabledControl("Auto Wall", this, &AimbotAutoWall);

	AutoRevoler.SetFileId("aim_AutoRevoler");
	AimbotGroup.PlaceLabledControl("Auto Revolver", this, &AutoRevoler);

	Resolver.SetFileId("tgt_Resolver");
	AimbotGroup.PlaceLabledControl("Anti Aim Resolver", this, &Resolver);

	Sinister.SetFileId("tgt_Resolver");
	AimbotGroup.PlaceLabledControl("Anti Aim Correction", this, &Sinister);

	AccuracyBrute.SetFileId("acc_brute");
	AccuracyBrute.AddItem("Off");
	AccuracyBrute.AddItem("LBY Brute");
	AccuracyBrute.AddItem("LBY Auto");
	AccuracyBrute.AddItem("Delta");
	AimbotGroup.PlaceLabledControl("Lower Body Resolver", this, &AccuracyBrute);

	LByFIX.SetFileId("aa_fixed");
	AimbotGroup.PlaceLabledControl("Lower Body Correction", this, &LByFIX);

	AimbotSelection.SetFileId("tgt_selection");
	AimbotSelection.AddItem("Cycle");
	AimbotSelection.AddItem("Distance");
	AimbotSelection.AddItem("Lowest Health");
	AimbotGroup.PlaceLabledControl("Target Selection Mode", this, &AimbotSelection);

	AimbotHitbox.SetFileId("tgt_hitbox");
	AimbotHitbox.AddItem("Head");
	AimbotHitbox.AddItem("Neck");
	AimbotHitbox.AddItem("Chest");
	AimbotHitbox.AddItem("Stomach");
	AimbotHitbox.AddItem("Legs");
	AimbotGroup.PlaceLabledControl("Hitbox", this, &AimbotHitbox);

	AimbotMultipoint.SetFileId("tgt_multipoint");
	AimbotGroup.PlaceLabledControl("Multi-Point", this, &AimbotMultipoint);

	AimbotPointscale.SetFileId("tgt_pointscale");
	AimbotPointscale.SetBoundaries(0, 10);
	AimbotPointscale.SetValue(5.f);
	AimbotGroup.PlaceLabledControl("Point Scale", this, &AimbotPointscale);

	AimbotHitscan.SetFileId("tgt_hitscan");
	AimbotHitscan.AddItem("Off"); //0
	AimbotHitscan.AddItem("Body, Head"); // 1
	AimbotHitscan.AddItem("Head, Body, Legs"); // 2
	AimbotHitscan.AddItem("Everything"); // 3
	AimbotHitscan.AddItem("Body, Toes"); // 4
	AimbotGroup.PlaceLabledControl("Multi-Point", this, &AimbotHitscan);

	AimbotBaimHP.SetFileId("acc_baimhp");
	AimbotBaimHP.SetBoundaries(0, 100);
	AimbotBaimHP.SetValue(0);
	AimbotGroup.PlaceLabledControl("Baim Under X HP", this, &AimbotBaimHP);

	AimbotSilentAim.SetFileId("tgt_hitscan2");
	AimbotSilentAim.AddItem("Off");
	AimbotSilentAim.AddItem("Silent");
	AimbotSilentAim.AddItem("Perfect Silent");
	AimbotGroup.PlaceLabledControl("Silent Aim", this, &AimbotSilentAim);

	AimbotHitchance.SetFileId("acc_chance");
	AimbotHitchance.SetBoundaries(0, 100);
	AimbotHitchance.SetValue(0);
	AimbotGroup.PlaceLabledControl("Minimum Hit Chance", this, &AimbotHitchance);

	AimbotFov.SetFileId("aim_fov");
	AimbotFov.SetBoundaries(0.f, 180.f);
	AimbotFov.SetValue(39.f);
	AimbotGroup.PlaceLabledControl("FOV Range", this, &AimbotFov);

	AimbotMinimumDamage.SetFileId("acc_mindmg");
	AimbotMinimumDamage.SetBoundaries(1.f, 99.f);
	AimbotMinimumDamage.SetValue(1.f);
	AimbotGroup.PlaceLabledControl("Minimum Damage", this, &AimbotMinimumDamage);
#pragma endregion Aimbot Controls Get Setup in here

#pragma region Accuracy
	AccuracyGroup.SetPosition(370, 329);
	AccuracyGroup.SetText("Accuracy");
	AccuracyGroup.SetSize(310, 220); //280
	RegisterControl(&AccuracyGroup);

	AccuracyRecoil.SetFileId("acc_norecoil");
	AccuracyGroup.PlaceLabledControl("Anti Recoil", this, &AccuracyRecoil);

	AccuracyNoSpread.SetFileId("acc_nospread");
	AccuracyGroup.PlaceLabledControl("Anti Spread", this, &AccuracyNoSpread);

	AccuracyAutoStop.SetFileId("acc_stop");
	AccuracyGroup.PlaceLabledControl("Auto Stop", this, &AccuracyAutoStop);

	AccuracyAutoScope.SetFileId("acc_scope");
	AccuracyGroup.PlaceLabledControl("Auto Scope", this, &AccuracyAutoScope);

	AccuracyResolverYaw.SetFileId("acc_aaa");
	AccuracyResolverYaw.AddItem("Off");
	AccuracyResolverYaw.AddItem("On");
	AccuracyGroup.PlaceLabledControl("Resolver", this, &AccuracyResolverYaw);

	AccuracyPositionAdjustment.SetFileId("acc_posadj");
	AccuracyGroup.PlaceLabledControl("Lag Correction", this, &AccuracyPositionAdjustment);

	AccuracyPrediction.SetFileId("acc_predi");
	AccuracyGroup.PlaceLabledControl("Accuracy Prediction", this, &AccuracyPrediction);
#pragma endregion  Accuracy controls get Setup in here

#pragma region AntiAim
	AntiAimGroup.SetPosition(370, 53); //344
	AntiAimGroup.SetText("Anti-Aimbot");
	AntiAimGroup.SetSize(310, 245);
	RegisterControl(&AntiAimGroup);

	AntiAimEnable.SetFileId("aa_enable");
	AntiAimGroup.PlaceLabledControl("Enable", this, &AntiAimEnable);

	AntiAimPitch.SetFileId("aa_x");
	AntiAimPitch.AddItem("None");
	AntiAimPitch.AddItem("Emotion");
	AntiAimPitch.AddItem("Down");
	AntiAimPitch.AddItem("Up");
	AntiAimPitch.AddItem("Zero");
	AntiAimPitch.AddItem("Mixed");
	AntiAimGroup.PlaceLabledControl("Pitch", this, &AntiAimPitch);

	AntiAimYaw.SetFileId("aa_y");
	AntiAimYaw.AddItem("None");
	AntiAimYaw.AddItem("Jitter");
	AntiAimYaw.AddItem("180 Jitter");
	AntiAimYaw.AddItem("LBY Backwards");
	AntiAimYaw.AddItem("Back Switch");
	AntiAimYaw.AddItem("Backwards Flip");
	AntiAimYaw.AddItem("Random Flip");
	AntiAimYaw.AddItem("Sideways Jitter");
	AntiAimYaw.AddItem("Arrow Keys");
	AntiAimGroup.PlaceLabledControl("Real Yaw", this, &AntiAimYaw);

	AntiAimOffset.SetFileId("aa_offset");
	AntiAimOffset.SetBoundaries(0, 360);
	AntiAimOffset.SetValue(0);
	AntiAimGroup.PlaceLabledControl("Offset", this, &AntiAimOffset);

	FakeYawAA.SetFileId("aa_fy");
	FakeYawAA.AddItem("None");
	FakeYawAA.AddItem("Fake Backwards");
	FakeYawAA.AddItem("Fake Sideways");
	FakeYawAA.AddItem("Fake Jitter");
	FakeYawAA.AddItem("Fake LBY Jitter");
	FakeYawAA.AddItem("Fake Spin");
	FakeYawAA.AddItem("Fake 180 Jitter");
	FakeYawAA.AddItem("Fake Lowerbody");
	FakeYawAA.AddItem("Fake Flip");
	AntiAimGroup.PlaceLabledControl("Fake Yaw", this, &FakeYawAA);

	AntiAimFakeOffset.SetFileId("aa_fakeoffset");
	AntiAimFakeOffset.SetBoundaries(0, 360);
	AntiAimFakeOffset.SetValue(0);
	AntiAimGroup.PlaceLabledControl("Offset", this, &AntiAimFakeOffset);

	AntiAimEdge.SetFileId("aa_edge");
	AntiAimEdge.AddItem("None");
	AntiAimEdge.AddItem("Normal");
	AntiAimGroup.PlaceLabledControl("Edge", this, &AntiAimEdge);

	AntiAimTarget.SetFileId("aa_target");
	AntiAimGroup.PlaceLabledControl("Anti Aim At Target", this, &AntiAimTarget);

	EdgeAntiAim.SetFileId("aa_EdgeAntiAim");
	AntiAimGroup.PlaceLabledControl("Fake Peek", this, &EdgeAntiAim);

#pragma endregion  AntiAim controls get setup in here
}

/*void CLegitBotTab::Setup()
{
SetTitle("G");

ActiveLabel.SetPosition(16, -14);
ActiveLabel.SetText("Active");
RegisterControl(&ActiveLabel);

Active.SetFileId("active");
Active.SetPosition(66, -14);
RegisterControl(&Active);

#pragma region Aimbot
AimbotGroup.SetPosition(16, 16);
AimbotGroup.SetText("Aimbot");
AimbotGroup.SetSize(240, 210);
RegisterControl(&AimbotGroup);

AimbotEnable.SetFileId("aim_enable");
AimbotGroup.PlaceLabledControl("Enable", this, &AimbotEnable);

AimbotAutoFire.SetFileId("aim_autofire");
AimbotGroup.PlaceLabledControl("Auto Fire", this, &AimbotAutoFire);

//AimbotFriendlyFire.SetFileId("aim_friendfire");
//AimbotGroup.PlaceLabledControl("Friendly Fire", this, &AimbotFriendlyFire);

AimbotKeyPress.SetFileId("aim_usekey");
AimbotGroup.PlaceLabledControl("On Key Press", this, &AimbotKeyPress);

AimbotKeyBind.SetFileId("aim_key");
AimbotGroup.PlaceLabledControl("Key Bind", this, &AimbotKeyBind);

AimbotAutoPistol.SetFileId("aim_apistol");
AimbotGroup.PlaceLabledControl("Auto Pistol", this, &AimbotAutoPistol);

#pragma endregion Aimbot shit

#pragma region Triggerbot
TriggerGroup.SetPosition(272, 16);
TriggerGroup.SetText("Triggerbot");
TriggerGroup.SetSize(240, 210);
RegisterControl(&TriggerGroup);

TriggerEnable.SetFileId("trig_enable");
TriggerGroup.PlaceLabledControl("Enable", this, &TriggerEnable);

TriggerKeyPress.SetFileId("trig_onkey");
TriggerGroup.PlaceLabledControl("On Key Press", this, &TriggerKeyPress);

TriggerKeyBind.SetFileId("trig_key");
TriggerGroup.PlaceLabledControl("Key Bind", this, &TriggerKeyBind);

TriggerDelay.SetFileId("trig_time");
TriggerDelay.SetBoundaries(0.f, 1000.f);
TriggerGroup.PlaceLabledControl("Delay (ms)", this, &TriggerDelay);
#pragma endregion Triggerbot stuff

WeaponMainGroup.SetPosition(16, 250);
WeaponMainGroup.SetText("Rifles");
WeaponMainGroup.SetSize(240, 175);
RegisterControl(&WeaponMainGroup);

WeaponMainHitbox.SetFileId("main_hitbox");
WeaponMainHitbox.AddItem("Head");
WeaponMainHitbox.AddItem("Neck");
WeaponMainHitbox.AddItem("Chest");
WeaponMainHitbox.AddItem("Stomach");
WeaponMainHitbox.AddItem("Multihitbox");
WeaponMainGroup.PlaceLabledControl("Hitbox", this, &WeaponMainHitbox);

WeaponMainSpeed.SetFileId("main_speed");
WeaponMainSpeed.SetBoundaries(0.f, 100.f);
WeaponMainSpeed.SetValue(1.f);
WeaponMainGroup.PlaceLabledControl("Max speed", this, &WeaponMainSpeed);

WeaponMainFoV.SetFileId("main_fov");
WeaponMainFoV.SetBoundaries(0.0f, 30.0f);
WeaponMainFoV.SetValue(3.0f);
WeaponMainGroup.PlaceLabledControl("FOV", this, &WeaponMainFoV);

WeaponMainRecoil.SetFileId("main_recoil");
WeaponMainRecoil.SetBoundaries(0.f, 2.f);
WeaponMainRecoil.SetValue(1.f);
WeaponMainGroup.PlaceLabledControl("Recoil", this, &WeaponMainRecoil);

WeaponMainAimtime.SetFileId("main_aimtime");
WeaponMainAimtime.SetBoundaries(0, 3);
WeaponMainAimtime.SetValue(0);
WeaponMainGroup.PlaceLabledControl("Aim Time", this, &WeaponMainAimtime);

WeaoponMainStartAimtime.SetFileId("main_aimstart");
WeaoponMainStartAimtime.SetBoundaries(0, 5);
WeaoponMainStartAimtime.SetValue(0);
WeaponMainGroup.PlaceLabledControl("Start Aim Time", this, &WeaoponMainStartAimtime);
#pragma endregion

#pragma region Pistols
WeaponPistGroup.SetPosition(272, 250);
WeaponPistGroup.SetText("Pistols");
WeaponPistGroup.SetSize(240, 175);
RegisterControl(&WeaponPistGroup);

WeaponPistHitbox.SetFileId("pist_hitbox");
WeaponPistHitbox.AddItem("Head");
WeaponPistHitbox.AddItem("Neck");
WeaponPistHitbox.AddItem("Chest");
WeaponPistHitbox.AddItem("Stomach");
WeaponPistHitbox.AddItem("Multihitbox");
WeaponPistGroup.PlaceLabledControl("Hitbox", this, &WeaponPistHitbox);

WeaponPistSpeed.SetFileId("pist_speed");
WeaponPistSpeed.SetBoundaries(0.f, 100.f);
WeaponPistSpeed.SetValue(1.0f);
WeaponPistGroup.PlaceLabledControl("Max Speed", this, &WeaponPistSpeed);

WeaponPistFoV.SetFileId("pist_fov");
WeaponPistFoV.SetBoundaries(0.0f, 30.0f);
WeaponPistFoV.SetValue(3.0f);
WeaponPistGroup.PlaceLabledControl("FOV", this, &WeaponPistFoV);

WeaponPistRecoil.SetFileId("pist_recoil");
WeaponPistRecoil.SetBoundaries(0.f, 2.f);
WeaponPistRecoil.SetValue(1.f);
WeaponPistGroup.PlaceLabledControl("Recoil", this, &WeaponPistRecoil);

WeaponPistAimtime.SetFileId("pist_aimtime");
WeaponPistAimtime.SetBoundaries(0, 3);
WeaponPistAimtime.SetValue(0);
WeaponPistGroup.PlaceLabledControl("Aim Time", this, &WeaponPistAimtime);

WeaoponPistStartAimtime.SetFileId("pist_aimstart");
WeaoponPistStartAimtime.SetBoundaries(0, 5);
WeaoponPistStartAimtime.SetValue(0);
WeaponPistGroup.PlaceLabledControl("Start Aim Time", this, &WeaoponPistStartAimtime);
#pragma endregion

#pragma region Snipers
WeaponSnipGroup.SetPosition(528, 250);
WeaponSnipGroup.SetText("Snipers");
WeaponSnipGroup.SetSize(240, 175);
RegisterControl(&WeaponSnipGroup);

WeaponSnipHitbox.SetFileId("snip_hitbox");
WeaponSnipHitbox.AddItem("Head");
WeaponSnipHitbox.AddItem("Neck");
WeaponSnipHitbox.AddItem("Chest");
WeaponSnipHitbox.AddItem("Stomach");
WeaponSnipHitbox.AddItem("Multihitbox");
WeaponSnipGroup.PlaceLabledControl("Hitbox", this, &WeaponSnipHitbox);

WeaponSnipSpeed.SetFileId("snip_speed");
WeaponSnipSpeed.SetBoundaries(0.f, 100.f);
WeaponSnipSpeed.SetValue(1.5f);
WeaponSnipGroup.PlaceLabledControl("Max Speed", this, &WeaponSnipSpeed);

WeaponSnipFoV.SetFileId("snip_fov");
WeaponSnipFoV.SetBoundaries(0.0f, 30.0f);
WeaponSnipFoV.SetValue(2.0f);
WeaponSnipGroup.PlaceLabledControl("FOV", this, &WeaponSnipFoV);

WeaponSnipRecoil.SetFileId("snip_recoil");
WeaponSnipRecoil.SetBoundaries(0.f, 2.f);
WeaponSnipRecoil.SetValue(1.f);
WeaponSnipGroup.PlaceLabledControl("Recoil", this, &WeaponSnipRecoil);

WeaponSnipAimtime.SetFileId("snip_aimtime");
WeaponSnipAimtime.SetBoundaries(0, 3);
WeaponSnipAimtime.SetValue(0);
WeaponSnipGroup.PlaceLabledControl("Aim Time", this, &WeaponSnipAimtime);

WeaoponSnipStartAimtime.SetFileId("pist_aimstart");
WeaoponSnipStartAimtime.SetBoundaries(0, 5);
WeaoponSnipStartAimtime.SetValue(0);
WeaponSnipGroup.PlaceLabledControl("Start Aim Time", this, &WeaoponSnipStartAimtime);
#pragma endregion
*/
/*#pragma region MPs
WeaponMpGroup.SetPosition(16, 458);
WeaponMpGroup.SetText("MPs");
WeaponMpGroup.SetSize(240, 176);
RegisterControl(&WeaponMpGroup);

WeaponMpHitbox.SetFileId("mps_hitbox");
WeaponMpHitbox.AddItem("Head");
WeaponMpHitbox.AddItem("Neck");
WeaponMpHitbox.AddItem("Chest");
WeaponMpHitbox.AddItem("Stomach");
WeaponMpHitbox.AddItem("Multihitbox");
WeaponMpGroup.PlaceLabledControl("Hitbox", this, &WeaponMpHitbox);

WeaponMpSpeed.SetFileId("mps_speed");
WeaponMpSpeed.SetBoundaries(0.f, 100.f);
WeaponMpSpeed.SetValue(1.0f);
WeaponMpGroup.PlaceLabledControl("Max Speed", this, &WeaponMpSpeed);

WeaponMpFoV.SetFileId("mps_fov");
WeaponMpFoV.SetBoundaries(0.0f, 30.0f);
WeaponMpFoV.SetValue(4.0f);
WeaponMpGroup.PlaceLabledControl("FOV", this, &WeaponMpFoV);

WeaponMpRecoil.SetFileId("mps_recoil");
WeaponMpRecoil.SetBoundaries(0.f, 2.f);
WeaponMpRecoil.SetValue(1.f);
WeaponMpGroup.PlaceLabledControl("Recoil", this, &WeaponMpRecoil);

WeaponMpAimtime.SetFileId("mps_aimtime");
WeaponMpAimtime.SetBoundaries(0, 3);
WeaponMpAimtime.SetValue(0);
WeaponMpGroup.PlaceLabledControl("Aim Time", this, &WeaponMpAimtime);

WeaoponMpStartAimtime.SetFileId("mps_aimstart");
WeaoponMpStartAimtime.SetBoundaries(0, 5);
WeaoponMpStartAimtime.SetValue(0);
WeaponMpGroup.PlaceLabledControl("Start Aim Time", this, &WeaoponMpStartAimtime);
#pragma endregion

#pragma region Shotguns
WeaponShotgunGroup.SetPosition(272, 458);
WeaponShotgunGroup.SetText("Shotguns");
WeaponShotgunGroup.SetSize(240, 176);
RegisterControl(&WeaponShotgunGroup);

WeaponShotgunHitbox.SetFileId("shotgun_hitbox");
WeaponShotgunHitbox.AddItem("Head");
WeaponShotgunHitbox.AddItem("Neck");
WeaponShotgunHitbox.AddItem("Chest");
WeaponShotgunHitbox.AddItem("Stomach");
WeaponShotgunHitbox.AddItem("Multihitbox");
WeaponShotgunGroup.PlaceLabledControl("Hitbox", this, &WeaponShotgunHitbox);

WeaponShotgunSpeed.SetFileId("shotgun_speed");
WeaponShotgunSpeed.SetBoundaries(0.f, 100.f);
WeaponShotgunSpeed.SetValue(1.0f);
WeaponShotgunGroup.PlaceLabledControl("Max Speed", this, &WeaponShotgunSpeed);

WeaponShotgunFoV.SetFileId("shotgun_fov");
WeaponShotgunFoV.SetBoundaries(0.0f, 30.0f);
WeaponShotgunFoV.SetValue(3.0f);
WeaponShotgunGroup.PlaceLabledControl("FOV", this, &WeaponShotgunFoV);

WeaponShotgunRecoil.SetFileId("shotgun_recoil");
WeaponShotgunRecoil.SetBoundaries(0.f, 2.f);
WeaponShotgunRecoil.SetValue(1.f);
WeaponShotgunGroup.PlaceLabledControl("Recoil", this, &WeaponShotgunRecoil);

WeaponShotgunAimtime.SetFileId("shotgun_aimtime");
WeaponShotgunAimtime.SetBoundaries(0, 3);
WeaponShotgunAimtime.SetValue(0);
WeaponShotgunGroup.PlaceLabledControl("Aim Time", this, &WeaponShotgunAimtime);

WeaoponShotgunStartAimtime.SetFileId("shotgun_aimstart");
WeaoponShotgunStartAimtime.SetBoundaries(0, 5);
WeaoponShotgunStartAimtime.SetValue(0);
WeaponShotgunGroup.PlaceLabledControl("Start Aim Time", this, &WeaoponShotgunStartAimtime);
#pragma endregion

#pragma region Machineguns
WeaponMGGroup.SetPosition(528, 458);
WeaponMGGroup.SetText("Machineguns");
WeaponMGGroup.SetSize(240, 176);
RegisterControl(&WeaponMGGroup);

WeaponMGHitbox.SetFileId("mg_hitbox");
WeaponMGHitbox.AddItem("Head");
WeaponMGHitbox.AddItem("Neck");
WeaponMGHitbox.AddItem("Chest");
WeaponMGHitbox.AddItem("Stomach");
WeaponMGHitbox.AddItem("Multihitbox");
WeaponMGGroup.PlaceLabledControl("Hitbox", this, &WeaponMGHitbox);

WeaponMGSpeed.SetFileId("mg_speed");
WeaponMGSpeed.SetBoundaries(0.f, 100.f);
WeaponMGSpeed.SetValue(1.0f);
WeaponMGGroup.PlaceLabledControl("Max Speed", this, &WeaponMGSpeed);
WeaponMGFoV.SetFileId("mg_fov");
WeaponMGFoV.SetBoundaries(0.0f, 30.0f);
WeaponMGFoV.SetValue(4.0f);
WeaponMGGroup.PlaceLabledControl("FOV", this, &WeaponMGFoV);

WeaponMGRecoil.SetFileId("mg_recoil");
WeaponMGRecoil.SetBoundaries(0.f, 2.f);
WeaponMGRecoil.SetValue(1.f);
WeaponMGGroup.PlaceLabledControl("Recoil", this, &WeaponMGRecoil);

WeaponMGAimtime.SetFileId("mg_aimtime");
WeaponMGAimtime.SetBoundaries(0, 3);
WeaponMGAimtime.SetValue(0);
WeaponMGGroup.PlaceLabledControl("Aim Time", this, &WeaponMGAimtime);

WeaoponMGStartAimtime.SetFileId("mg_aimstart");
WeaoponMGStartAimtime.SetBoundaries(0, 5);
WeaoponMGStartAimtime.SetValue(0);
WeaponMGGroup.PlaceLabledControl("Start Aim Time", this, &WeaoponMGStartAimtime)*/;
//}

void CVisualTab::Setup()
{
	SetTitle("C");

	ActiveLabel.SetPosition(75, 30);
	ActiveLabel.SetText("Active");
	RegisterControl(&ActiveLabel);

	Active.SetFileId("active");
	Active.SetPosition(60, 30);
	RegisterControl(&Active);

#pragma region Options
	OptionsGroup.SetText("Player ESP");
	OptionsGroup.SetPosition(60, 65);
	OptionsGroup.SetSize(185, 425);
	RegisterControl(&OptionsGroup);

	FiltersEnemiesOnly.SetFileId("ftr_enemyonly");
	OptionsGroup.PlaceLabledControl("Enemies Only", this, &FiltersEnemiesOnly);

	OptionsBox.SetFileId("opt_box");
	OptionsGroup.PlaceLabledControl("Box", this, &OptionsBox);

	OptionsChams.SetFileId("opt_chams");
	OptionsChams.AddItem("Off");
	OptionsChams.AddItem("Normal");
	OptionsChams.AddItem("Flat");
	OptionsGroup.PlaceLabledControl("Chams", this, &OptionsChams);

	Skeleton.SetFileId("opt_box");
	OptionsGroup.PlaceLabledControl("Skeleton", this, &Skeleton);

	OptionsAimSpot.SetFileId("opt_aimspot");
	OptionsGroup.PlaceLabledControl("Head Cross", this, &OptionsAimSpot);

	OptionsHealth.SetFileId("opt_hp");
	OptionsGroup.PlaceLabledControl("Health", this, &OptionsHealth);

	OptionsWeapon.SetFileId("otr_recoilhair");
	OptionsGroup.PlaceLabledControl("Weapon", this, &OptionsWeapon);

	//DrawMoney.SetFileId("otr_backtomexico4");
	//OptionsGroup.PlaceLabledControl("Ammo", this, &DrawMoney);

	//Distanse.SetFileId("opt_Distaa");
	//OptionsGroup.PlaceLabledControl("Distance", this, &Distanse);

	OptionsName.SetFileId("opt_name");
	OptionsGroup.PlaceLabledControl("Name", this, &OptionsName);

	OptionsInfo.SetFileId("opt_info");
	OptionsGroup.PlaceLabledControl("Armor", this, &OptionsInfo);

	OptionsArmur.SetFileId("opt_armor");
	OptionsGroup.PlaceLabledControl("Armor Bar", this, &OptionsArmur);

	OptionsPlant.SetFileId("opt_bone");
	OptionsGroup.PlaceLabledControl("Bomb", this, &OptionsPlant);

	LBY.SetFileId("opt_LBY");
	OptionsGroup.PlaceLabledControl("Enemy LBY", this, &LBY);

#pragma endregion Setting up the Options controls

#pragma region Other
	OtherGroup.SetText("Other ESP");
	OtherGroup.SetPosition(280, 65);
	OtherGroup.SetSize(334, 200);
	RegisterControl(&OtherGroup);

	OtherViewmodelFOV.SetFileId("otr_viewfov");
	OtherViewmodelFOV.SetBoundaries(0.f, 180.f);
	OtherViewmodelFOV.SetValue(0.f);
	OtherGroup.PlaceLabledControl("Viewmodel FOV Changer", this, &OtherViewmodelFOV);

	OtherFOV.SetFileId("otr_fov");
	OtherFOV.SetBoundaries(0.f, 180.f);
	OtherFOV.SetValue(90.f);
	OtherGroup.PlaceLabledControl("Field of View Changer", this, &OtherFOV);

	Lines.SetFileId("otr_line");
	OtherGroup.PlaceLabledControl("Snap Lines", this, &Lines);

	lbycheck.SetFileId("otr_backtomexico5");
	OtherGroup.PlaceLabledControl("LBY Indicator", this, &lbycheck);

	canhit.SetFileId("otr_backtomexico6");
	OtherGroup.PlaceLabledControl("Hit Check", this, &canhit);

	Nades.SetFileId("otr_backtomexico7");
	OtherGroup.PlaceLabledControl("Nade ESP", this, &Nades);

	Grenades.SetFileId("otr_backtomexico2");
	OtherGroup.PlaceLabledControl("Grenade Prediction", this, &Grenades);

#pragma endregion Setting up the Other controls

#pragma region Effects
	EffectsGroup.SetText("Effects");
	EffectsGroup.SetPosition(280, 295);
	EffectsGroup.SetSize(334, 195);
	RegisterControl(&EffectsGroup);

	OtherNoVisualRecoil.SetFileId("otr_NoVisualRecoil");
	EffectsGroup.PlaceLabledControl("No Visual Recoil", this, &OtherNoVisualRecoil);

	OtherNoScope.SetFileId("otr_NoScope");
	EffectsGroup.PlaceLabledControl("No Scope", this, &OtherNoScope);

	OtherHitmarker.SetFileId("otr_backtomexico3");
	EffectsGroup.PlaceLabledControl("Hitmarker", this, &OtherHitmarker);

	OtherNightMode.SetFileId("Otr_NightMode");
	EffectsGroup.PlaceLabledControl("Night Mode", this, &OtherNightMode);

	OtherNoFlash.SetFileId("otr_noflash");
	EffectsGroup.PlaceLabledControl("Anti Flash", this, &OtherNoFlash);

	OtherNoSmoke.SetFileId("otr_nosmoke");
	EffectsGroup.PlaceLabledControl("Anti Smoke", this, &OtherNoSmoke);

	AmbientSkybox.SetFileId("AmbientSkybox");
	AmbientSkybox.AddItem("Disabled");
	AmbientSkybox.AddItem("Night");
	EffectsGroup.PlaceLabledControl("Skybox", this, &AmbientSkybox);

#pragma endregion Setting up the Other controls
}

void CMiscTab::Setup()
{
	SetTitle("B");

#pragma region Other
	OtherGroup.SetPosition(60, 53);
	OtherGroup.SetSize(360, 340);
	OtherGroup.SetText("Miscellaneous");
	RegisterControl(&OtherGroup);

	OtherAutoJump.SetFileId("otr_autojump");
	OtherAutoJump.AddItem("Disabled");
	OtherAutoJump.AddItem("On");
	OtherGroup.PlaceLabledControl("Bunnyhop", this, &OtherAutoJump);

	OtherEdgeJump.SetFileId("otr_edgejump");
	OtherGroup.PlaceLabledControl("Edge Jump", this, &OtherEdgeJump);

	OtherAutoStrafe.SetFileId("otr_strafe");
	OtherAutoStrafe.AddItem("Disabled");
	OtherAutoStrafe.AddItem("Legit");
	OtherAutoStrafe.AddItem("Rage");
	OtherGroup.PlaceLabledControl("Auto Strafe", this, &OtherAutoStrafe);

	OtherSafeMode.SetFileId("otr_safemode");
	OtherSafeMode.SetState(true);
	OtherGroup.PlaceLabledControl("Anti Untrust", this, &OtherSafeMode);

	OtherSpectators.SetFileId("otr_speclist");
	OtherGroup.PlaceLabledControl("Spectators List", this, &OtherSpectators);

	OtherClantag.SetFileId("otr_spam");
	OtherClantag.AddItem("Disabled");
	OtherClantag.AddItem("Paradise");
	OtherGroup.PlaceLabledControl("Custom Clantag", this, &OtherClantag);

	OtherAirStuck.SetFileId("otr_astuck");
	OtherGroup.PlaceLabledControl("Air Stuck", this, &OtherAirStuck);
	
	OtherThirdperson.SetFileId("aa_thirdpsr1");
	OtherGroup.PlaceLabledControl("Thirdperson Key", this, &OtherThirdperson);

	OtherCircleStrafe.SetFileId("otr_circlestrafe");
	OtherGroup.PlaceLabledControl("Circle Strafe", this, &OtherCircleStrafe);

	OtherCircleStrafeKey.SetFileId("otr_circlestrafeKey");
	OtherGroup.PlaceLabledControl("Circle Strafe Key", this, &OtherCircleStrafeKey);

	FakeWalk.SetFileId("otr_circlestrafe2");
	OtherGroup.PlaceLabledControl("Fake Walk", this, &FakeWalk);

#pragma endregion other random options

	ButtonGroup.SetPosition(450, 53);
	ButtonGroup.SetSize(195, 100);
	ButtonGroup.SetText("Presets");
	RegisterControl(&ButtonGroup);

	SaveButton.SetText("Save");
	SaveButton.SetCallback(SaveCallbk);
	ButtonGroup.PlaceOtherControl("Save", this, &SaveButton);

	LoadButton.SetText("Load");
	LoadButton.SetCallback(LoadCallbk);
	ButtonGroup.PlaceOtherControl("Load", this, &LoadButton);

	//Options


	SetNr.AddItem("Legit");
	SetNr.AddItem("Rage");
	SetNr.AddItem("Semi-Rage");
	SetNr.AddItem("HVH");
	SetNr.AddItem("Disable All");
	ButtonGroup.PlaceOtherControl("Config", this, &SetNr);

#pragma region FakeLag
	FakeLagGroup.SetPosition(60, 415);
	FakeLagGroup.SetSize(360, 141);
	FakeLagGroup.SetText("Fake-Lag");
	RegisterControl(&FakeLagGroup);

	FakeLagEnable.SetFileId("fakelag_enable");
	FakeLagGroup.PlaceLabledControl("Fake Lag", this, &FakeLagEnable);

	FakeLagChoke.SetFileId("fakelag_choke");
	FakeLagChoke.SetBoundaries(0, 16);
	FakeLagChoke.SetValue(0);
	FakeLagGroup.PlaceLabledControl("Choke Factor", this, &FakeLagChoke);

	FakeLagSend.SetFileId("fakelag_send");
	FakeLagSend.SetBoundaries(0, 16);
	FakeLagSend.SetValue(0);
	FakeLagGroup.PlaceLabledControl("Send Factor", this, &FakeLagSend);

	ChokeRandomize.SetFileId("choke_random");
	FakeLagGroup.PlaceLabledControl("Randomize Choke", this, &ChokeRandomize);

	SendRandomize.SetFileId("send_random");
	FakeLagGroup.PlaceLabledControl("Randomize Send", this, &SendRandomize);
#pragma endregion fakelag shit

	/*#pragma region Teleport
	TeleportGroup.SetPosition(13, 160);
	TeleportGroup.SetSize(280, 75);
	TeleportGroup.SetText("Teleport");
	RegisterControl(&TeleportGroup);

	TeleportEnable.SetFileId("teleport_enable");
	TeleportGroup.PlaceOtherControl("Enable", this, &TeleportEnable);

	TeleportKey.SetFileId("teleport_key");
	TeleportGroup.PlaceOtherControl("Key", this, &TeleportKey);

	#pragma endregion*/




	//Options

	/*#pragma region OverideFov
	FOVGroup.SetPosition(16, 365);
	FOVGroup.SetSize(360, 75);
	FOVGroup.SetText("FOV Changer");
	RegisterControl(&FOVGroup);

	FOVEnable.SetFileId("fov_enable");
	FOVGroup.PlaceLabledControl("Enable", this, &FOVEnable);

	FOVSlider.SetFileId("fov_slider");
	FOVSlider.SetBoundaries(0, 200);
	FOVSlider.SetValue(0);
	FOVGroup.PlaceLabledControl("FOV Amount", this, &FOVSlider);

	#pragma endregion*/
}

/*void CPlayersTab::Setup()
{
SetTitle("J");

#pragma region PList

pListGroup.SetPosition(16, 16);
pListGroup.SetSize(680, 200);
pListGroup.SetText("Player List");
pListGroup.SetColumns(2);
RegisterControl(&pListGroup);

pListPlayers.SetPosition(26, 46);
pListPlayers.SetSize(640, 50);
pListPlayers.SetHeightInItems(20);
RegisterControl(&pListPlayers);

#pragma endregion

#pragma region Options

OptionsGroup.SetPosition(16, 257);
OptionsGroup.SetSize(450, 120);
OptionsGroup.SetText("Player Options");
RegisterControl(&OptionsGroup);

OptionsFriendly.SetFileId("pl_friendly");
OptionsGroup.PlaceLabledControl("Friendly", this, &OptionsFriendly);

OptionsAimPrio.SetFileId("pl_priority");
OptionsGroup.PlaceLabledControl("Priority", this, &OptionsAimPrio);

OptionsCalloutSpam.SetFileId("pl_callout");
OptionsGroup.PlaceLabledControl("Callout Spam", this, &OptionsCalloutSpam);

#pragma endregion
}

DWORD GetPlayerListIndex(int EntId)
{
player_info_t pinfo;
Interfaces::Engine->GetPlayerInfo(EntId, &pinfo);

// Bot
if (pinfo.guid[0] == 'B' && pinfo.guid[1] == 'O')
{
char buf[64]; sprintf_s(buf, "BOT_420%sAY", pinfo.name);
return CRC32(buf, 64);
}
else
{
return CRC32(pinfo.guid, 32);
}
}

bool IsFriendly(int EntId)
{
DWORD plistId = GetPlayerListIndex(EntId);
if (PlayerList.find(plistId) != PlayerList.end())
{
return PlayerList[plistId].Friendly;
}

return false;
}

bool IsAimPrio(int EntId)
{
DWORD plistId = GetPlayerListIndex(EntId);
if (PlayerList.find(plistId) != PlayerList.end())
{
return PlayerList[plistId].AimPrio;
}

return false;
}

bool IsCalloutTarget(int EntId)
{
DWORD plistId = GetPlayerListIndex(EntId);
if (PlayerList.find(plistId) != PlayerList.end())
{
return PlayerList[plistId].Callout;
}

return false;
}

void UpdatePlayerList()
{
IClientEntity* pLocal = Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());
if (Interfaces::Engine->IsConnected() && Interfaces::Engine->IsInGame() && pLocal && pLocal->IsAlive())
{
Menu::Window.Playerlist.pListPlayers.ClearItems();

// Loop through all active entitys
for (int i = 0; i < Interfaces::EntList->GetHighestEntityIndex(); i++)
{
// Get the entity

player_info_t pinfo;
if (i != Interfaces::Engine->GetLocalPlayer() && Interfaces::Engine->GetPlayerInfo(i, &pinfo))
{
IClientEntity* pEntity = Interfaces::EntList->GetClientEntity(i);
int HP = 100; char* Location = "Unknown";
char *Friendly = " ", *AimPrio = " ";

DWORD plistId = GetPlayerListIndex(Menu::Window.Playerlist.pListPlayers.GetValue());
if (PlayerList.find(plistId) != PlayerList.end())
{
Friendly = PlayerList[plistId].Friendly ? "Friendly" : "";
AimPrio = PlayerList[plistId].AimPrio ? "AimPrio" : "";
}

if (pEntity && !pEntity->IsDormant())
{
HP = pEntity->GetHealth();
Location = pEntity->GetLastPlaceName();
}

char nameBuffer[512];
sprintf_s(nameBuffer, "%-24s %-10s %-10s [%d HP] [Last Seen At %s]", pinfo.name, IsFriendly(i) ? "Friend" : " ", IsAimPrio(i) ? "AimPrio" : " ", HP, Location);
Menu::Window.Playerlist.pListPlayers.AddItem(nameBuffer, i);

}

}

DWORD meme = GetPlayerListIndex(Menu::Window.Playerlist.pListPlayers.GetValue());

// Have we switched to a different player?
static int PrevSelectedPlayer = 0;
if (PrevSelectedPlayer != Menu::Window.Playerlist.pListPlayers.GetValue())
{
if (PlayerList.find(meme) != PlayerList.end())
{
Menu::Window.Playerlist.OptionsFriendly.SetState(PlayerList[meme].Friendly);
Menu::Window.Playerlist.OptionsAimPrio.SetState(PlayerList[meme].AimPrio);
Menu::Window.Playerlist.OptionsCalloutSpam.SetState(PlayerList[meme].Callout);

}
else
{
Menu::Window.Playerlist.OptionsFriendly.SetState(false);
Menu::Window.Playerlist.OptionsAimPrio.SetState(false);
Menu::Window.Playerlist.OptionsCalloutSpam.SetState(false);

}
}
PrevSelectedPlayer = Menu::Window.Playerlist.pListPlayers.GetValue();

PlayerList[meme].Friendly = Menu::Window.Playerlist.OptionsFriendly.GetState();
PlayerList[meme].AimPrio = Menu::Window.Playerlist.OptionsAimPrio.GetState();
PlayerList[meme].Callout = Menu::Window.Playerlist.OptionsCalloutSpam.GetState();
}
}*/

void Menu::SetupMenu()
{
	Window.Setup();

	GUI.RegisterWindow(&Window);
	GUI.BindWindow(VK_INSERT, &Window);
}

void Menu::DoUIFrame()
{
	// General Processing

	// If the "all filter is selected tick all the others
	if (Window.VisualsTab.FiltersAll.GetState())
	{
		Window.VisualsTab.FiltersC4.SetState(true);
		Window.VisualsTab.FiltersChickens.SetState(true);
		Window.VisualsTab.FiltersPlayers.SetState(true);
		Window.VisualsTab.FiltersWeapons.SetState(true);
	}

	GUI.Update();
	GUI.Draw();


}


