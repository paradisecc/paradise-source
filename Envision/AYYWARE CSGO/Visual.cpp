/*
Syn's AyyWare Framework 2015
*/

#include "Visuals.h"
#include "Interfaces.h"
#include "RenderManager.h"

void CVisuals::Init()
{
}

void CVisuals::Move(CUserCmd *pCmd, bool &bSendPacket) {}

// Main ESP Drawing loop
void CVisuals::Draw()
{
	// No Scope
	OtherNoScope();

}

void CVisuals::OtherNoScope()
{
	if (Menu::Window.VisualsTab.OtherNoScope.GetState())
	{
		if (hackManager.pLocal()->IsScoped() && hackManager.pLocal()->IsAlive())
		{
			int Width;
			int Height;
			Interfaces::Engine->GetScreenSize(Width, Height);

			Color cColor = Color(0, 0, 0, 255);
			Render::Line(Width / 2, 0, Width / 2, Height, cColor);
			Render::Line(0, Height / 2, Width, Height / 2, cColor);


		}
		else
		{

		}
	}
}

void CVisuals::DrawCrosshair()
{
	RECT View = Render::GetViewport();
	int MidX = View.right / 2;
	int MidY = View.bottom / 2;

	Render::Line(MidX - 10, MidY, MidX + 10, MidY, Color(40, 115, 214, 255));
	Render::Line(MidX, MidY - 10, MidX, MidY + 10, Color(40, 115, 214, 255));
}

void CVisuals::DefaultCrosshair()
{
	IClientEntity *pLocal = hackManager.pLocal();

	if (!pLocal->IsScoped() && pLocal->IsAlive())
	{
		ConVar* cross = Interfaces::CVar->FindVar("weapon_debug_spread_show");
		SpoofedConvar* cross_spoofed = new SpoofedConvar(cross);
		cross_spoofed->SetInt(3);
	}
}

//Draw Sniper Crosshair
void CVisuals::DrawNormalSniperCrosshair()
{
	RECT View = Render::GetViewport();
	int MidX = View.right / 2;
	int MidY = View.bottom / 2;
	//Render::Line(MidX - 1, MidY / 512, MidX - 1, MidY * 2, Color(0, 0, 0, 255));
	Render::Line(MidX, MidY / 512, MidX, MidY * 2, Color(0, 0, 0, 255));

	//Render::Line(MidX / 512, MidY - 1, MidX * 2, MidY - 1, Color(0, 0, 0, 255));
	Render::Line(MidX / 512, MidY, MidX * 2, MidY, Color(0, 0, 0, 255));
}
