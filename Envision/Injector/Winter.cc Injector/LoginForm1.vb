﻿Imports System.Management
Imports System
Imports System.Text
Imports System.Security.Cryptography
Imports System.Security.Permissions
Imports System.Net
Imports System.Net.HttpWebRequest
Imports System.Net.HttpWebResponse
Imports System.IO
Imports System.Threading.Tasks
Imports System.Windows
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Public Class LoginForm1
    ' TODO: Insert code to perform custom authentication using the provided username and password 
    ' (See http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' The custom principal can then be attached to the current thread's principal as follows: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' where CustomPrincipal is the IPrincipal implementation used to perform authentication. 
    ' Subsequently, My.User will return identity information encapsulated in the CustomPrincipal object
    ' such as the username, display name, etc.

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Try
            Dim licenseTestRequest As WebRequest = WebRequest.Create("https://winterhook.cc/forums/applications/nexus/interface/licenses/index.php?activate")
            licenseTestRequest.Method = "POST"
            Dim post2Data As String = "key=" & PasswordTextBox.Text & "&identifier=" & UsernameTextBox.Text
            Dim byte2Array As Byte() = Encoding.UTF8.GetBytes(post2Data)
            licenseTestRequest.ContentType = "application/x-www-form-urlencoded"
            licenseTestRequest.ContentLength = byte2Array.Length
            Dim data2Stream As Stream = licenseTestRequest.GetRequestStream()
            data2Stream.Write(byte2Array, 0, byte2Array.Length)
            data2Stream.Close()
            Dim myHttpWebResponse As HttpWebResponse = CType(licenseTestRequest.GetResponse(), HttpWebResponse)
            myHttpWebResponse.Close()
        Catch ex As WebException
            If ex.Status = WebExceptionStatus.ProtocolError And CType(ex.Response, HttpWebResponse).StatusCode = 400 Then
                MsgBox("Waiting...")
                Exit Try
            ElseIf ex.Status = WebExceptionStatus.ProtocolError Then
                MsgBox("Other error. HTTP code:" & CType(ex.Response, HttpWebResponse).StatusCode)
                Me.Close()
                End
            End If
        End Try
        Try
            Dim licenseTestRequest As WebRequest = WebRequest.Create("https://winterhook.cc/forums/applications/nexus/interface/licenses/index.php?check")
            licenseTestRequest.Method = "POST"
            Dim post2Data As String = "key=" & PasswordTextBox.Text & "&identifier=" & UsernameTextBox.Text & "&usage_id=1"
            Dim byte2Array As Byte() = Encoding.UTF8.GetBytes(post2Data)
            licenseTestRequest.ContentType = "application/x-www-form-urlencoded"
            licenseTestRequest.ContentLength = byte2Array.Length
            Dim data2Stream As Stream = licenseTestRequest.GetRequestStream()
            data2Stream.Write(byte2Array, 0, byte2Array.Length)
            data2Stream.Close()
            Dim myHttpWebResponse As HttpWebResponse = CType(licenseTestRequest.GetResponse(), HttpWebResponse)
            myHttpWebResponse.Close()
        Catch ex As WebException
            If ex.Status = WebExceptionStatus.ProtocolError And CType(ex.Response, HttpWebResponse).StatusCode = 400 Then
                MsgBox("Invalid Information")
                Me.Close()
                End
            ElseIf ex.Status = WebExceptionStatus.ProtocolError Then
                MsgBox("Other error. HTTP code:" & CType(ex.Response, HttpWebResponse).StatusCode)
                Me.Close()
                End
            End If
        End Try
        Dim licenseRequest As WebRequest = WebRequest.Create("https://winterhook.cc/forums/applications/nexus/interface/licenses/index.php?check")
        licenseRequest.Method = "POST"
        Dim postData As String = "key=" & PasswordTextBox.Text & "&identifier=" & UsernameTextBox.Text & "&usage_id=1"
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
        licenseRequest.ContentType = "application/x-www-form-urlencoded"
        licenseRequest.ContentLength = byteArray.Length
        Dim dataStream As Stream = licenseRequest.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim response As WebResponse = licenseRequest.GetResponse()
        dataStream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        dataStream.Close()
        response.Close()
        If CType(response, HttpWebResponse).StatusDescription = "OK" Then
            Form1.Show()
            Me.Hide()
        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        End
    End Sub

    Private Sub LoginForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim hw As New clsComputerInfo

        Dim hdd As String
        Dim cpu As String
        Dim mb As String
        Dim mac As String

        cpu = hw.GetProcessorId()
        hdd = hw.GetVolumeSerial("C")
        mb = hw.GetMotherBoardID()
        mac = hw.GetMACAddress()

        MsgBox(cpu & "   " & hdd & "   " & mb & "   " & mac)

        Dim hwid As String = Strings.UCase(hw.getMD5Hash(cpu & hdd & mb & mac))
        MessageBox.Show(Strings.UCase(hwid))

        If Not (hwid = "8BDC0A730C4395DF47C20CBDC0E6A668") Then
            MsgBox("Invalid HWID.")
            End
        End If
    End Sub

    Private Sub UsernameLabel_Click(sender As Object, e As EventArgs) Handles UsernameLabel.Click

    End Sub
End Class
Public Class clsComputerInfo

    Friend Function GetProcessorId() As String
        Dim strProcessorId As String = String.Empty
        Dim query As New SelectQuery("Win32_processor")
        Dim search As New ManagementObjectSearcher(query)
        Dim info As ManagementObject

        For Each info In search.Get()
            strProcessorId = info("processorId").ToString()
        Next
        Return strProcessorId

    End Function

    Friend Function GetMACAddress() As String

        Dim mc As ManagementClass = New ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim moc As ManagementObjectCollection = mc.GetInstances()
        Dim MACAddress As String = String.Empty
        For Each mo As ManagementObject In moc

            If (MACAddress.Equals(String.Empty)) Then
                If CBool(mo("IPEnabled")) Then MACAddress = mo("MacAddress").ToString()

                mo.Dispose()
            End If
            MACAddress = MACAddress.Replace(":", String.Empty)

        Next
        Return MACAddress
    End Function

    Friend Function GetVolumeSerial(Optional ByVal strDriveLetter As String = "C") As String

        Dim disk As ManagementObject = New ManagementObject(String.Format("win32_logicaldisk.deviceid=""{0}:""", strDriveLetter))
        disk.Get()
        Return disk("VolumeSerialNumber").ToString()
    End Function

    Friend Function GetMotherBoardID() As String

        Dim strMotherBoardID As String = String.Empty
        Dim query As New SelectQuery("Win32_BaseBoard")
        Dim search As New ManagementObjectSearcher(query)
        Dim info As ManagementObject
        For Each info In search.Get()

            strMotherBoardID = info("SerialNumber").ToString()

        Next
        Return strMotherBoardID

    End Function



    Friend Function getMD5Hash(ByVal strToHash As String) As String
        Dim md5Obj As New Security.Cryptography.MD5CryptoServiceProvider
        Dim bytesToHash() As Byte = System.Text.Encoding.ASCII.GetBytes(strToHash)

        bytesToHash = md5Obj.ComputeHash(bytesToHash)

        Dim strResult As String = ""

        For Each b As Byte In bytesToHash
            strResult += b.ToString("x2")
        Next

        Return strResult
    End Function


End Class
